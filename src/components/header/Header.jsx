import React, { useState } from "react";
import "./header.css";

import { Link } from "react-router-dom";

const Header = ({ userName }) => {
  const [Mobile, setMobile] = useState(false);

  const user2 = localStorage.getItem("user");
  const user1 = JSON.parse(user2);
  // let user = userName
  // console.log(userName);
  let user = userName ? userName : user1;

  const logOut = () => {
    localStorage.removeItem("user");
    window.location.reload(false);
  };
  return (
    <>
      <header>
        <div className="container flexSB">
          <nav className="flexSB">
            <div className="logo">
              <img src="./images/logo.png" alt="..." />
            </div>
            {/*<ul className='flexSB'>*/}
            <ul
              className={Mobile ? "navMenu-list" : "flexSB"}
              onClick={() => setMobile(false)}
            >
              <li>
                <a href="/">Home</a>
              </li>
              <li>
                <a href="/">Series</a>
              </li>
              <li>
                <a href="/">Movies</a>
              </li>
              <li>
                <a href="/">Pages</a>
              </li>
              <li>
                <a href="/">Pricing</a>
              </li>
              <li>
                <a href="/">Contact</a>
              </li>
            </ul>
            <button className="toggle" onClick={() => setMobile(!Mobile)}>
              {Mobile ? (
                <i className="fa fa-times"></i>
              ) : (
                <i className="fa fa-bars"></i>
              )}
            </button>
          </nav>
          <div className="account flexSB">
            <i className="fa fa-search"></i>
            <i class="fas fa-bell"></i>
            {user ? user : <i className="fas fa-user"></i>}
            {user ? (
              <button className="log-out" onClick={logOut}>log out</button>
            ) : (
              <Link className="link" to="login">Login</Link>
            )}
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
