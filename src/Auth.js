import React, { useState } from "react";
import "./Auth.css";
import { Link } from "react-router-dom";

const Auth = ({ setUser }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const signUpBtn = () => {
    if (username !== "" && password !== "") {
      localStorage.setItem("user", JSON.stringify(username));
      setUser(username);
    }
  };

  return (
    <div className="login">
      <input
        type="text"
        placeholder="username"
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <input
        type="password"
        placeholder="password"
        onChange={(e) => {
          setUsername(e.target.value);
        }}
      />
      <span>
        <Link to='/' onClick={signUpBtn}>submit</Link>
      </span>
    </div>
  );
};

export default Auth;
