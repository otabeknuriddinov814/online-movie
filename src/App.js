import "./App.css"
import HomePage from "./home/HomePage"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import SinglePage from "./components/watch/SinglePage"
import Header from "./components/header/Header"
import Footer from "./components/footer/Footer"
import Auth from "./Auth"
import { useState } from "react"

function App() {
  const [user, setUser] = useState("")

  return (
    <>
      <Router>
        <Header userName={user}/>
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='login' />
             <Auth setUser={setUser}/>
          <Route/>
          <Route path='/singlepage/:id' component={SinglePage} exact />
        </Switch>
        <Footer />
      </Router>
    </>
  )
}

export default App
